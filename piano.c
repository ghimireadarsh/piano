#include <graphics.h>
#include <stdlib.h>
#include <stdio.h>
#include <conio.h>
#include <dos.h>
#include <math.h>
void redrawpaino()
{
  int i;
   /* set the fill style */
   setfillstyle(1,BLACK);

   /* draw the 3-d bar */
   bar3d(0, 20,500,200, 10, 1);

   setfillstyle(1,YELLOW);
//   for(i=24;i<460;i+=24)
   bar(24,100,480,200);
    // line
   setcolor(BLUE);
   for(i=24;i<=480;i+=24)
   {
	line(i,100,i,200);
   }
   setfillstyle(1,BLUE);
   bar(38,100,58,160);
   bar(62,100,82,160);
   bar(86,100,106,160);
   bar(134,100,154,160);
   bar(158,100,178,160);
   bar(206,100,226,160);
   bar(230,100,250,160);
   bar(254,100,274,160);
   bar(302,100,322,160);
   bar(326,100,346,160);
   bar(374,100,394,160);
   bar(398,100,418,160);
   bar(422,100,442,160);


}

void tunetime(char *x, char *y,int k)
{
	int l,i=0;
	char a,b;
	do{
	a=x[i];
	b=y[i];
	// octave 1 right most part
	if(a=='c' && b=='0')
	{
		sound(32);
	}
	else if(a=='d'&& b==',')
	{
		sound(36);
	}
	else if(a=='d'&& b=='0')
	{
		sound(40);

	}
	else if(a=='e'&& b==',')
	{
		sound(39);

	}
	else if(a=='e'&& b=='0')
	{
		sound(42);
	}
	else if(a=='f'&& b=='0')
	{
		sound(44);
	}
	else if(a=='g'&& b==',')
	{
		sound(47);

	}
	else if(a=='g'&& b=='0')
	{
		sound(49);
	}
	else if(a=='a'&& b==',')
	{
		sound(52);
	}
	else if(a=='a'&& b=='0')
	{
		sound(55);
	}
	else if(a=='b'&& b==',')
	{
		sound(59);

	}
	else if(a=='b'&& b=='0')
	{
		sound(62);
	}
			 //octave 4 middle part
	else if(a=='c'&& b=='1')
	{
		sound(250);
	}
	else if(a=='d'&& b=='.')
	{
		sound(280);
	}
	else if(a=='d'&& b=='1')
	{
		sound(295);
	}
	else if(a=='e'&& b=='.')
	{
		sound(310);
	}
	else if(a=='e'&& b=='1')
	{
		sound(330);
	}
	else if(a=='f'&& b=='1')
	{
		sound(350);
	}
	else if(a=='g'&& b=='.')
	{
		sound(370);
	}
	else if(a=='g'&& b=='1')
	{
		sound(390);
	}
	else if(a=='a'&& b=='.')
	{
		sound(415);
	}
	else if(a=='a'&& b=='1')
	{
		sound(440);
	}
	else if(a=='b'&& b=='.')
	{
		sound(470);
	}
	else if(a=='b'&& b=='1')
	{
		sound(500);
	}


	//octave 6 left most part
	else if(a=='e'&& b=='2')
	{
		sound(1200);
	}
	else if(a=='f'&& b=='2')
	{
		sound(1400);
	}
	else if(a=='g'&& b=='/')
	{
		sound(1480);
	}
	else if(a=='g'&& b=='2')
	{
		sound(1550);
	}
	else if(a=='a'&& b=='/')
	{
		sound(1670);
	}
	else if(a=='a'&& b=='2')
	{
		sound(1760);
	}
	else if(a=='b'&& b=='/')
	{
		sound(1870);
	}
	else if(a=='b'&& b=='2')
	{
		sound(1975);
	}
	else
	continue;
	delay(500);
	nosound();
	i++;
	k--;
	setcolor(RED);
	circle(100,300,50);
	delay(10);
	setcolor(BLUE);
	circle(150,200,60);
	delay(10);
	setcolor(YELLOW);
	circle(170,300,50);
	delay(10);
	setcolor(WHITE);
	circle(300,100,60);
	delay(10);
	setcolor(RED);
	circle(400,200,50);
	delay(10);
	setcolor(BLUE);
	circle(500,300,60);
	delay(10);
	setcolor(WHITE);
	circle(600,400,50);
	delay(10);
	setcolor(YELLOW);
	circle(300,300,60);
	delay(10);
	cleardevice();
	}while(k!=0);
	exit(1);
}

void playpaino(char *x, char *y, int k)
{
	char a,b;
	k=0;
	do{
	a=getch();
	b=getch();
	x[k]=a; y[k]=b;
	// octave 1 right most part
	if(a=='c' && b=='0')
	{
		sound(32);
		setfillstyle(5,BLACK);
		bar(456,100,480,200);
	}
	else if(a=='d'&& b==',')
	{
		sound(36);
		setfillstyle(5,BLACK);
		bar(422,100,442,160);
	}
	else if(a=='d'&& b=='0')
	{
		sound(40);
		setfillstyle(5,BLACK);
		bar(432,100,456,200);
	}
	else if(a=='e'&& b==',')
	{
		sound(39);
		setfillstyle(5,BLACK);
		bar(398,100,418,160);

	}
	else if(a=='e'&& b=='0')
	{
		sound(42);
		setfillstyle(5,BLACK);
		bar(408,100,432,200);
	}
	else if(a=='f'&& b=='0')
	{
		sound(44);
		setfillstyle(5,BLACK);
		bar(384,100,408,200);
	}
	else if(a=='g'&& b==',')
	{
		sound(47);
		setfillstyle(5,BLACK);
		bar(374,100,394,160);

	}
	else if(a=='g'&& b=='0')
	{
		sound(49);
		setfillstyle(5,BLACK);
		bar(360,100,384,200);
	}
	else if(a=='a'&& b==',')
	{
		sound(52);
		setfillstyle(5,BLACK);
		bar(326,100,346,160);

	}
	else if(a=='a'&& b=='0')
	{
		sound(55);
		setfillstyle(5,BLACK);
		bar(336,100,360,200);
	}
	else if(a=='b'&& b==',')
	{
		sound(59);
		setfillstyle(5,BLACK);
		bar(302,100,322,160);

	}
	else if(a=='b'&& b=='0')
	{
		sound(62);
		setfillstyle(5,BLACK);
		bar(312,100,336,200);
	}
			 //octave 4 middle part
	else if(a=='c'&& b=='1')
	{
		sound(250);
		setfillstyle(5,BLACK);
		bar(288,100,312,200);
	}
	else if(a=='d'&& b=='.')
	{
		sound(280);
		setfillstyle(5,BLACK);
		bar(254,100,274,160);
	}
	else if(a=='d'&& b=='1')
	{
		sound(295);
		setfillstyle(5,BLACK);
		bar(264,100,288,200);
	}
	else if(a=='e'&& b=='.')
	{
		sound(310);
		setfillstyle(5,BLACK);
		bar(230,100,250,160);
	}
	else if(a=='e'&& b=='1')
	{
		sound(330);
		setfillstyle(5,BLACK);
		bar(240,100,264,200);
	}
	else if(a=='f'&& b=='1')
	{
		sound(350);
		setfillstyle(5,BLACK);
		bar(216,100,240,200);
	}
	else if(a=='g'&& b=='.')
	{
		sound(370);
		setfillstyle(5,BLACK);
		bar(206,100,226,160);
	}
	else if(a=='g'&& b=='1')
	{
		sound(390);
		setfillstyle(5,BLACK);
		bar(192,100,216,200);
	}
	else if(a=='a'&& b=='.')
	{
		sound(415);
		setfillstyle(5,BLACK);
		bar(158,100,178,160);
	}
	else if(a=='a'&& b=='1')
	{
		sound(440);
		setfillstyle(5,BLACK);
		bar(168,100,192,200);
	}
	else if(a=='b'&& b=='.')
	{
		sound(470);
		setfillstyle(5,BLACK);
		bar(134,100,154,160);
	}
	else if(a=='b'&& b=='1')
	{
		sound(500);
		setfillstyle(5,BLACK);
		bar(144,100,168,200);
	}


	//octave 6 left most part
	else if(a=='e'&& b=='2')
	{
		sound(1200);
		setfillstyle(5,BLACK);
		bar(120,100,144,200);
	}
	else if(a=='f'&& b=='2')
	{
		sound(1400);
		setfillstyle(5,BLACK);
		bar(96,100,120,200);
	}
	else if(a=='g'&& b=='/')
	{
		sound(1480);
		setfillstyle(5,BLACK);
		bar(86,100,106,160);
	}
	else if(a=='g'&& b=='2')
	{
		sound(1550);
		setfillstyle(5,BLACK);
		bar(72,100,96,200);
	}
	else if(a=='a'&& b=='/')
	{
		sound(1670);
		setfillstyle(5,BLACK);
		bar(62,100,82,160);
	}
	else if(a=='a'&& b=='2')
	{
		sound(1760);
		setfillstyle(5,BLACK);
		bar(48,100,72,200);
	}
	else if(a=='b'&& b=='/')
	{
		sound(1870);
		setfillstyle(5,BLACK);
		bar(38,100,58,160);
	}
	else if(a=='b'&& b=='2')
	{
		sound(1975);
		setfillstyle(5,BLACK);
		bar(24,100,48,200);
	}
	delay(100);
	redrawpaino();
	delay(500);
	nosound();
	k++;
	}while(a!='q');
	x[k]=NULL;
	y[k]=NULL;
}

void drawpaino()
{
  int i;
   /* set the fill style */
   setfillstyle(1,BLACK);

   /* draw the 3-d bar */
   bar3d(0, 20,500,200, 10, 1);

   setfillstyle(1,YELLOW);
//   for(i=24;i<460;i+=24)
   bar(24,100,480,200);
    // line
   setcolor(BLUE);
   for(i=24;i<=480;i+=24)
   {
	line(i,100,i,200);
   }
   setfillstyle(1,BLUE);
   bar(38,100,58,160);
   bar(62,100,82,160);
   bar(86,100,106,160);
   bar(134,100,154,160);
   bar(158,100,178,160);
   bar(206,100,226,160);
   bar(230,100,250,160);
   bar(254,100,274,160);
   bar(302,100,322,160);
   bar(326,100,346,160);
   bar(374,100,394,160);
   bar(398,100,418,160);
   bar(422,100,442,160);
}

int main(void)
{
   /* request auto detection */
   int gdriver = DETECT, gmode, errorcode;
   int i,k;
   char c;
   char x[100], y[100];
   /* initialize graphics and local variables */
   initgraph(&gdriver, &gmode, "C://TC//BGI");

   /* read result of initialization */
   errorcode = graphresult();
   if (errorcode != grOk)  /* an error occurred */
   {
      printf("Graphics error: %s\n", grapherrormsg(errorcode));
      printf("Press any key to halt:");
      getch();
      exit(1); /* terminate with an error code */
   }
   /* clean up */
   printf("\n");
   printf("Do you want to play paino? \n ['y' for yes and 'n' for no] ");
   scanf("%c",&c);
   cleardevice();
   if(c=='y')
   {
       drawpaino();
   }
   else
   { 	printf("\n Thanks for seeing me!!!");
   }
   playpaino(x,y,k);
   cleardevice();
   printf("Do you want to listen your tune? \n ('y' for yes and 'n' for no)");
   c=getch();
   cleardevice();
   if(c=='y')
   {
	  tunetime(x,y,k);
   }
   else if(c=='n')
   {
	printf("Thanks for your time");
   }
   printf("\n Do you want to listen again?");
   c=getch();
   if(c=='y')
   {
	  tunetime(x,y,k);
   }
   else if(c=='n')
   {
	printf("Thanks for your time");
   }

   getch();
   closegraph();
   return 0;
}
